################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../board/board.c \
../board/clock_config.c \
../board/peripherals.c \
../board/pin_mux.c 

OBJS += \
./board/board.o \
./board/clock_config.o \
./board/peripherals.o \
./board/pin_mux.o 

C_DEPS += \
./board/board.d \
./board/clock_config.d \
./board/peripherals.d \
./board/pin_mux.d 


# Each subdirectory must supply rules for building sources it contributes
board/%.o: ../board/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__NEWLIB__ -DSDK_OS_BAREMETAL -DFSL_RTOS_BM -DSDK_DEBUGCONSOLE=0 -DCPU_MKL27Z64VDA4_cm0plus -DCPU_MKL27Z64VDA4 -D__MCUXPRESSO -D__USE_CMSIS -DDEBUG -I../drivers -I../CMSIS -I../board -I../../MKL27Z_Libraries_Sources/usb/V100/hid/device -I../../MKL27Z_Libraries_Sources/usb/V100/hid/include -I../../MKL27Z_Libraries_Sources/usb/V100/hid/osa -I../../MKL27Z_Libraries_Sources/usb/V100/hid/source -I../../MKL27Z_Libraries_Sources/usb/V100 -I../../MKL27Z_Libraries_Sources/gpio -I../../MKL27Z_Libraries_Sources/JumpKboot -I../../MKL27Z_Libraries_Sources/pit -I../../MKL27Z_Libraries_Sources/uart -I../../MKL27Z_Libraries_Sources/FUSES -I../../MKL27Z_Libraries_Sources/flash -I../source -I../ -I../startup -Os -fno-common -g3 -Wall -c -ffunction-sections -fdata-sections -ffreestanding -fno-builtin -mcpu=cortex-m0plus -mthumb -D__NEWLIB__ -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


