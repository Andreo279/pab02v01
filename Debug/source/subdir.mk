################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../source/PAB02V01.cpp \
../source/PAB02uart.cpp \
../source/PAB02usb.cpp 

C_SRCS += \
../source/mtb.c \
../source/semihost_hardfault.c 

OBJS += \
./source/PAB02V01.o \
./source/PAB02uart.o \
./source/PAB02usb.o \
./source/mtb.o \
./source/semihost_hardfault.o 

CPP_DEPS += \
./source/PAB02V01.d \
./source/PAB02uart.d \
./source/PAB02usb.d 

C_DEPS += \
./source/mtb.d \
./source/semihost_hardfault.d 


# Each subdirectory must supply rules for building sources it contributes
source/%.o: ../source/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C++ Compiler'
	arm-none-eabi-c++ -D__NEWLIB__ -DSDK_OS_BAREMETAL -DFSL_RTOS_BM -DSDK_DEBUGCONSOLE=0 -DCPU_MKL27Z64VDA4_cm0plus -DCPU_MKL27Z64VDA4 -D__MCUXPRESSO -D__USE_CMSIS -DDEBUG -I../drivers -I../CMSIS -I../board -I../../MKL27Z_Libraries_Sources/usb/V100/hid/device -I../../MKL27Z_Libraries_Sources/usb/V100/hid/include -I../../MKL27Z_Libraries_Sources/usb/V100/hid/osa -I../../MKL27Z_Libraries_Sources/usb/V100/hid/source -I../../MKL27Z_Libraries_Sources/usb/V100 -I../../MKL27Z_Libraries_Sources/gpio -I../../MKL27Z_Libraries_Sources/JumpKboot -I../../MKL27Z_Libraries_Sources/pit -I../../MKL27Z_Libraries_Sources/uart -I../../MKL27Z_Libraries_Sources/FUSES -I../../MKL27Z_Libraries_Sources/flash -I../source -I../ -I../startup -Os -fno-common -g3 -Wall -c -fmessage-length=0 -mcpu=cortex-m0plus -mthumb -D__NEWLIB__ -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

source/%.o: ../source/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__NEWLIB__ -DSDK_OS_BAREMETAL -DFSL_RTOS_BM -DSDK_DEBUGCONSOLE=0 -DCPU_MKL27Z64VDA4_cm0plus -DCPU_MKL27Z64VDA4 -D__MCUXPRESSO -D__USE_CMSIS -DDEBUG -I../drivers -I../CMSIS -I../board -I../../MKL27Z_Libraries_Sources/usb/V100/hid/device -I../../MKL27Z_Libraries_Sources/usb/V100/hid/include -I../../MKL27Z_Libraries_Sources/usb/V100/hid/osa -I../../MKL27Z_Libraries_Sources/usb/V100/hid/source -I../../MKL27Z_Libraries_Sources/usb/V100 -I../../MKL27Z_Libraries_Sources/gpio -I../../MKL27Z_Libraries_Sources/JumpKboot -I../../MKL27Z_Libraries_Sources/pit -I../../MKL27Z_Libraries_Sources/uart -I../../MKL27Z_Libraries_Sources/FUSES -I../../MKL27Z_Libraries_Sources/flash -I../source -I../ -I../startup -Os -fno-common -g3 -Wall -c -ffunction-sections -fdata-sections -ffreestanding -fno-builtin -mcpu=cortex-m0plus -mthumb -D__NEWLIB__ -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


