/*
 * ControlPAB02V01.h
 *
 *  Created on: 19 de set de 2018
 *      Author: Eletronica2
 */

#ifndef PAB02UART_H_
#define PAB02UART_H_

#include "PAB02includes.h"

class PAB02uart
{
public:
	PAB02uart();
	void init(OriginUart_t origin, LPUART_Type *base);
	void setBaudrate(uint32_t baudrate);
	void setBufferRxUART(uint8_t data);
	void verifyUARTToSend();
	void read(uint8_t* headBuf, uint8_t* buf);
	void readBuffer(uint8_t* buf);
	void setHeaderUSB(uint8_t* buf);
	uint32_t available();
	void write(uint8_t* buf, uint32_t len);

	virtual ~PAB02uart();

private:

	V_Uart uart;

	uint8_t O_init;

	uint8_t O_bufferRx[_oRingBuffer][_bufferLen];

	uint32_t O_indexRx[_oRingBuffer];

	uint32_t O_timeOutRx[_oRingBuffer];

	uint8_t O_sending[_oRingBuffer];

	uint32_t O_sendingIndexBuffer[_oRingBuffer];

	uint32_t O_bufferIndex;

	uint32_t O_sendingBufferIndex;

	uint32_t O_BCRx[_oRingBuffer];

	uint32_t O_RxUARTChannel[_oRingBuffer];

	uint8_t O_origin;

	LPUART_Type *O_base;

	struct O_typeProtocol
	{
		enum
		{
			E_control = 1, E_config, E_message
		};
	};

	O_typeProtocol O_typeP;
};

#endif /* PAB02UART_H_ */
