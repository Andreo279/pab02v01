#include <PAB02uart.h>
#include <PAB02usb.h>
#include <stdio.h>
#include "board.h"
#include "peripherals.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "MKL27Z644.h"
#include "V_GPIO.h"
#include "V_USBHID.h"
#include "V_Uart.h"
#include "FUSES.h"
#include "V_FLASH.h"

#define _LED0 GPIOB, 0u, 0u, 1u
#define _LED1 GPIOB, 1u, 0u, 1u
#define _LED2 GPIOC, 1u, 0u, 0u
#define _LED3 GPIOC, 2u, 0u, 0u
#define _BTL GPIOA, 4u, 1u

uint32_t GBaudrate = 115200;

V_Obj_Flash flashBaudrate;

uint8_t MedelRevision[16] =
{ "PAB_02V01" };

#define MedelRevisionLen 16

uint8_t FirmwareVersion[16] =
{ "1.0" };

#define FirmwareVersionLen 16

PAB02uart externUart;
PAB02uart displayUart;
//V_USB_HID USB1;
PAB02usb USB;

V_GPIO LED[4];
V_GPIO BTL;

uint8_t bufferRxUSB[_bufferLen];

volatile uint32_t receivedUSB;

uint8_t receivedBuffer[_bufferLen];
uint8_t receivedBufferHeader[7];
uint32_t receivedBufferLenUSB;
uint32_t receivedBufferLen[2] =
{ 0, 0 };
uint32_t receivedBufferIndex = 0;
uint32_t lenReceivedBufferToSend = 0;

uint32_t USBDataLen = 0;
uint8_t USBDataDestiny = 0;
uint8_t USBDataType = 0;
uint8_t USBReceiveComplete = 0;

uint8_t sendUSBBuffer[BufferUSBLen];

void USB_HID_Interrupt_Recv(volatile uint8_t * Buff)
{
	uint8_t buf[BufferUSBLen];

	for (uint8_t xiI = 0; xiI < BufferUSBLen; xiI++)
	{
		buf[xiI] = Buff[xiI];
	}

	USB.addBuffer(buf);
}

void LPUART0_Receive(uint8_t data)
{
	externUart.setBufferRxUART(data);
}

void LPUART1_Receive(uint8_t data)
{
	displayUart.setBufferRxUART(data);
}

uint32_t CounterBlink[4];

void counterBlink(uint8_t nLed)
{
	++CounterBlink[nLed];
	if (CounterBlink[nLed] > 50000)
	{
		CounterBlink[nLed] = 0;
		LED[nLed] = !LED[nLed];
	}
}

uint32_t iBaudrate[3];

int main(void)
{
	/* Init board hardware. */
	BOARD_InitBootPins();
	BOARD_InitBootClocks();
	BOARD_InitBootPeripherals();

	LED[0].Init(_LED0);
	LED[1].Init(_LED1);
	LED[2].Init(_LED2);
	LED[3].Init(_LED3);

	LED[0] = 1;
	LED[1] = 1;
	LED[2] = 1;
	LED[3] = 1;

	for (uint32_t xiI = 0; xiI < 1000000; xiI++)
	{

	}

	externUart.init(E_externUart, LPUART0);
	displayUart.init(E_displayUart, LPUART1);

	externUart.setBaudrate(GBaudrate);
	displayUart.setBaudrate(GBaudrate);

	flashBaudrate.init(1u);

	flashBaudrate.read(iBaudrate);

	GBaudrate = iBaudrate[0];

	if (GBaudrate < 1200)
	{
		GBaudrate = 1200;
	}
	else if (GBaudrate > 921600)
	{
		GBaudrate = 921600;
	}

//	displayUart.write(iBaudrate, 20u);

	USB.init();

	while (1)
	{
		receivedBufferLen[0] = externUart.available();
		receivedBufferLen[1] = displayUart.available();

		if (receivedBufferLen[0])
		{
			LED[2] = !LED[2];
			receivedBufferLenUSB = receivedBufferLen[0];
			externUart.read(receivedBufferHeader, receivedBuffer);
			displayUart.write(receivedBuffer, receivedBufferLenUSB);

			USB.write(receivedBuffer, receivedBufferLenUSB, E_message, E_externUart);
//				sendHeaderUSB = 1;
		}

		if (receivedBufferLen[1])
		{
			LED[1] = !LED[1];
			receivedBufferLenUSB = receivedBufferLen[1];
			displayUart.read(receivedBufferHeader, receivedBuffer);

			externUart.write(receivedBuffer, receivedBufferLenUSB);

			USB.write(receivedBuffer, receivedBufferLenUSB, E_message, E_displayUart);
//				sendHeaderUSB = 1;
		}

		if (USB.available())
		{
			LED[0] = !LED[0];
			USBDataLen = USB.read(bufferRxUSB);
//			bufferRxUSB [USBDataLen + 1] = 0xFF;
//			bufferRxUSB [USBDataLen + 2] = USB.DataType();
//			bufferRxUSB [USBDataLen + 3] = USB.DataDestiny();
//			bufferRxUSB [USBDataLen + 4] = bufferRxUSB[0];
//			bufferRxUSB [USBDataLen + 5] = bufferRxUSB[1];
//			bufferRxUSB [USBDataLen + 6] = 0xFF;
//
//			USB.write(bufferRxUSB, USBDataLen + 6, E_message, 0x00);

			switch (USB.DataType())
			{
			case E_control:

				switch (bufferRxUSB[0])
				{
				case E_updateFirmware:
					boot_loader_start();
					break;
				case E_modelRevision:
					memcpy(&bufferRxUSB[1], MedelRevision, MedelRevisionLen);
					USB.write(bufferRxUSB, MedelRevisionLen + 1, E_control, 0x00);
					break;
				case E_firmwareVersion:
					memcpy(&bufferRxUSB[1], FirmwareVersion, FirmwareVersionLen);
					USB.write(bufferRxUSB, FirmwareVersionLen + 1, E_control, 0x00);
					break;
				case E_led:
					break;
				}
				break;
			case E_configuration:

				switch (bufferRxUSB[0])
				{
				case E_baudrate:
					switch (bufferRxUSB[1])
					{
					case E_write:
					{
						GBaudrate = (bufferRxUSB[2] << 16) | (bufferRxUSB[3] << 8) | bufferRxUSB[4];

//								USB.write(bufferRxUSB, 10u, E_configuration, 0x00);

						Pit.De_Init();
						flashBaudrate.edit(GBaudrate);

						Save_Flash;

						Pit.Enable_PIT();

//						flashBaudrate.read(iBaudrate);
//
//						GBaudrate = (iBaudrate[0] << 16) | (iBaudrate[1]) | iBaudrate[2];
//
//						if (GBaudrate < 1200)
//						{
//							GBaudrate = 1200;
//						}
//						else if (GBaudrate > 921600)
//						{
//							GBaudrate = 921600;
//						}
						externUart.setBaudrate(GBaudrate);
						displayUart.setBaudrate(GBaudrate);
//						bufferRxUSB[5] = ((GBaudrate >> 16) & 0xff);
//						bufferRxUSB[6] = ((GBaudrate >> 8) & 0xff);
//						bufferRxUSB[7] = (GBaudrate & 0xff);
//						USB.write(bufferRxUSB, 15u, E_configuration, statuses);

					}
						break;
					case E_read:
						bufferRxUSB[2] = ((GBaudrate >> 16) & 0xff);
						bufferRxUSB[3] = ((GBaudrate >> 8) & 0xff);
						bufferRxUSB[4] = (GBaudrate & 0xff);

						USB.write(bufferRxUSB, 5u, E_configuration, 0x00);

						break;
					}
					break;
				}
				break;
			case E_message:
				switch (USB.DataDestiny())
				{
				case E_externUart:
					externUart.write(bufferRxUSB, USBDataLen);
					break;
				case E_displayUart:
					displayUart.write(bufferRxUSB, USBDataLen);
					break;
				case E_allUart:
					externUart.write(bufferRxUSB, USBDataLen);
					displayUart.write(bufferRxUSB, USBDataLen);
					break;
				default:
					bufferRxUSB[USBDataLen] = 0xFF;
					externUart.write(bufferRxUSB, USBDataLen + 1);
					displayUart.write(bufferRxUSB, USBDataLen + 1);
					break;
				}
				break;
			}
		}

		counterBlink(3);

	}
	return 0;
}
