/*
 * PAB02usb.h
 *
 *  Created on: 21 de set de 2018
 *      Author: Eletronica2
 */

#ifndef PAB02USB_H_
#define PAB02USB_H_

#include "PAB02includes.h"

class PAB02usb
{
public:
	PAB02usb();
	virtual ~PAB02usb();
	void write(uint8_t* buf, uint32_t len);
	void write(uint8_t *buf, uint32_t len, uint8_t type, uint8_t OD = 0);
	uint32_t readHeader(uint8_t *buf);
	uint8_t messageType(uint8_t *buf);
	uint8_t addBuffer(uint8_t *buf);
	uint8_t DataDestiny();
	uint8_t available();
	uint8_t DataType();
	uint32_t read(uint8_t* buf);

	void init();

private:
	V_USB_HID O_USB;

	uint32_t O_USBDataLen;

	uint8_t O_USBDataDestiny;

	uint8_t O_USBDataType;

	uint32_t O_bufferRx[_bufferLen];

	uint32_t O_bufferIndex;

	uint8_t O_Available;

};

#endif /* PAB02USB_H_ */
