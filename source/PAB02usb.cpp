/*
 * PAB02usb.cpp
 *
 *  Created on: 21 de set de 2018
 *      Author: Eletronica2
 */

#include <PAB02usb.h>

PAB02usb::PAB02usb()
{

	O_USBDataLen = 0;
	O_USBDataDestiny = 0;
	O_USBDataType = 0;
	O_bufferIndex = 0;
	O_Available = 0;
}

void PAB02usb::init()
{
//	uart.Init(LPUART1, 115200);
	O_USB.Init();
}

void PAB02usb::write(uint8_t* buf, uint32_t len)
{
	while (len)
	{
		if (len > BufferUSBLen)
		{
			O_USB.Send(buf, BufferUSBLen);
			len -= BufferUSBLen;
		}
		else
		{
			O_USB.Send(buf, len);
			len = 0;
		}
	}
}

void PAB02usb::write(uint8_t *buf, uint32_t len, uint8_t type, uint8_t OD)
{
	uint8_t buff[64];

	len = len + HeaderBufferUSBLen;

	buff[E_indexP_BC4] = ((len >> 24) & 0xff);
	buff[E_indexP_BC3] = ((len >> 16) & 0xff);
	buff[E_indexP_BC2] = ((len >> 8) & 0xff);
	buff[E_indexP_BC1] = (len & 0xff);
	buff[E_indexP_OD] = OD;
	buff[E_indexP_type] = type;

	memcpy(&buff[E_indexP_dataInit], buf, DataLenInFirstBuff);

	O_USB.Send(buff, BufferUSBLen);

	if (len > DataLenInFirstBuff)
	{
		len = len + HeaderBufferUSBLen - DataLenInFirstBuff;
	}
}

uint32_t PAB02usb::readHeader(uint8_t *buf)
{
	O_USBDataLen = ((buf[E_indexP_BC4] << 24) & 0xff) | ((buf[E_indexP_BC3] << 16) & 0xff)
			| ((buf[E_indexP_BC2] << 8) & 0xff) | (buf[E_indexP_BC1] & 0xff);
	O_USBDataDestiny = buf[E_indexP_OD];
	O_USBDataType = buf[E_indexP_type];

	if (O_USBDataLen > BufferUSBLen)
	{
		O_USBDataLen -= E_indexP_dataInit;
		memcpy(O_bufferRx, &buf[E_indexP_dataInit], DataLenInFirstBuff);
		O_bufferIndex = DataLenInFirstBuff;
	}
	else
	{
		O_USBDataLen -= E_indexP_dataInit;
		memcpy(O_bufferRx, &buf[E_indexP_dataInit], O_USBDataLen);
		O_bufferIndex = O_USBDataLen;
	}
	return O_USBDataLen;
}

uint8_t PAB02usb::messageType(uint8_t *buf)
{
	uint8_t bufferToRead = O_USBDataLen - O_bufferIndex;

	if (bufferToRead)
	{
		if (bufferToRead > BufferUSBLen)
		{
			bufferToRead = BufferUSBLen;
		}
		O_bufferIndex += bufferToRead;
		memcpy(O_bufferRx, buf, bufferToRead);
	}

	if (O_bufferIndex == O_USBDataLen)
	{
		O_bufferIndex = 0;
		O_Available++;
	}
	return bufferToRead;
}

uint8_t PAB02usb::addBuffer(uint8_t * buf)
{
	uint8_t readLen = 0;

	if (O_bufferIndex == 0)
	{
		readHeader(buf);
	}

	switch (O_USBDataType)
	{
	case E_control:
		O_Available++;
		O_bufferIndex = 0;
		switch (O_bufferRx[0])
		{
		case E_updateFirmware:
			break;
		case E_modelRevision:
			break;
		case E_firmwareVersion:
			break;
		case E_led:
			break;
		}
		break;
	case E_configuration:
		O_Available++;
		O_bufferIndex = 0;

		switch (O_bufferRx[0])
		{
		case E_baudrate:
			break;
		}
		break;
	case E_message:
		readLen = messageType(buf);
		break;
	}
	return readLen;
}

uint8_t PAB02usb::available()
{
	return O_Available;
}

uint8_t PAB02usb::DataDestiny()
{
	return O_USBDataDestiny;
}

uint8_t PAB02usb::DataType()
{
	return O_USBDataType;
}

uint32_t PAB02usb::read(uint8_t* buf)
{
	memcpy(buf, O_bufferRx, O_USBDataLen);
	O_Available--;
	return O_USBDataLen;
}

PAB02usb::~PAB02usb()
{
}

