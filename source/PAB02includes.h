/*
 * PAB02includes.h
 *
 *  Created on: 21 de set de 2018
 *      Author: Eletronica2
 */

#ifndef PAB02INCLUDES_H_
#define PAB02INCLUDES_H_

#include <stdio.h>
#include "PIT.h"
#include "V_Uart.h"
#include "V_USBHID.h"
#include "bootldrstart.h"

#define _oRingBuffer 2

#define _bufferLen 1025

#define BufferUSBLen 64u
#define HeaderBufferUSBLen 6u
#define DataLenInFirstBuff 58u

enum indexP
{
	E_indexP_BC4 = 0x00, E_indexP_BC3, E_indexP_BC2, E_indexP_BC1, E_indexP_OD, E_indexP_type, E_indexP_dataInit
};

enum UsbFrameType
{
	E_control = 0x01, E_configuration, E_message
};

typedef enum OriginUart
{
	E_externUart = 0x01, E_displayUart, E_allUart,
} OriginUart_t;

enum control
{
	E_updateFirmware = 0x00, E_modelRevision, E_firmwareVersion, E_led
};

enum configuration
{
	E_baudrate = 0x01
};

enum direction
{
	E_write = 0x00, E_read
};

#endif /* PAB02INCLUDES_H_ */
