/*
 * ControlPAB02V01.cpp
 *
 *  Created on: 19 de set de 2018
 *      Author: Eletronica2
 */

#include <PAB02uart.h>

PAB02uart::PAB02uart()
{
	O_bufferIndex = 0;
	O_sendingBufferIndex = 0;
	O_init = 0;
	for (uint8_t xiI = 0; xiI < _oRingBuffer; xiI++)
	{
		O_indexRx[xiI] = 0;
		O_timeOutRx[xiI] = 0;
		O_sending[xiI] = 0;
		O_sendingIndexBuffer[xiI] = 0;
		O_BCRx[xiI] = 0;
		O_RxUARTChannel[xiI] = 0;
	}

	if (Pit.enabled() == 0)
	{
		Pit.Enable_PIT();
	}
}

void PAB02uart::init(OriginUart_t origin, LPUART_Type *base)
{
	O_origin = (uint8_t) origin;
	O_base = base;
}

void PAB02uart::setBaudrate(uint32_t baudrate)
{
	if (O_init)
	{
		uart.De_Init();
	}
	O_init = 1;
	uart.Init(O_base, baudrate);
}

void PAB02uart::setBufferRxUART(uint8_t data)
{
	O_timeOutRx[O_bufferIndex] = Pit + 10;

	O_bufferRx[O_bufferIndex][O_indexRx[O_bufferIndex]] = data;

	O_indexRx[O_bufferIndex]++;

}

void PAB02uart::verifyUARTToSend()
{
	if (O_indexRx[O_bufferIndex])
	{
		if (O_timeOutRx[O_bufferIndex] < Pit)
		{
			O_sending[O_bufferIndex] = 1;
			O_sendingIndexBuffer[O_bufferIndex] = O_indexRx[O_bufferIndex];
			O_sendingBufferIndex = O_bufferIndex;
			O_indexRx[O_bufferIndex] = 0;
			if (O_bufferIndex < (_oRingBuffer - 1))
			{
				O_bufferIndex++;
			}
			else
			{
				O_bufferIndex = 0;
			}
		}
	}
}

void PAB02uart::read(uint8_t* headBuf, uint8_t* buf)
{
	setHeaderUSB(headBuf);
	readBuffer(buf);
	O_sendingIndexBuffer[O_sendingBufferIndex] = 0;
}

void PAB02uart::readBuffer(uint8_t* buf)
{
	memcpy(buf, O_bufferRx[O_sendingBufferIndex], O_sendingIndexBuffer[O_sendingBufferIndex]);
}

void PAB02uart::setHeaderUSB(uint8_t* headbuf)
{
	O_sendingIndexBuffer[O_sendingBufferIndex] += E_indexP_dataInit;
	headbuf[E_indexP_BC4] = ((O_sendingIndexBuffer[O_sendingBufferIndex] >> 24) & 0xff);
	headbuf[E_indexP_BC3] = ((O_sendingIndexBuffer[O_sendingBufferIndex] >> 16) & 0xff);
	headbuf[E_indexP_BC2] = ((O_sendingIndexBuffer[O_sendingBufferIndex] >> 8) & 0xff);
	headbuf[E_indexP_BC1] = (O_sendingIndexBuffer[O_sendingBufferIndex] & 0xff);
	headbuf[E_indexP_OD] = O_origin;
	headbuf[E_indexP_type] = O_typeP.E_message;
}

uint32_t PAB02uart::available()
{
	verifyUARTToSend();
	return O_sendingIndexBuffer[O_sendingBufferIndex];
}

void PAB02uart::write(uint8_t* buf, uint32_t len)
{
	uart.Write(buf, len);
}

PAB02uart::~PAB02uart()
{
	// TODO Auto-generated destructor stub
}

